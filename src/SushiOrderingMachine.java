import javax.swing.*;
import javax.swing.JPanel;
import javax.swing.JButton;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class SushiOrderingMachine {

    private JPanel root;
    private JPanel buttonpanel;
    private JButton Button0;
    private JButton Button1;
    private JButton Button2;
    private JButton Button3;
    private JButton Button4;
    private JButton Button5;
    private JLabel Details;
    private JTextArea orderDetails;
    private JButton orderConfirmedButton;
    private JTextArea orderHistory;
    private JButton cancelButton;
    private JLabel totalText;
    private JButton cheakoutbutton;
    int totalprice = 0;
    int preconfirmprice = 0;
    int[] cuntsushi = {0,0,0,0,0,0};
    int[] confirmedcuntsushi = {0,0,0,0,0,0};
    //注文画面の左上から順にメニュー名、価格を決め、配列に格納
    String[] menu = {"Otoro","Tyutoro","Tuna","Tyawanmushi","Ice Cream","Shotyu"};
    int[] price = {870,670,470,200,150,980};




//注文画面を押したときの処理
    void order (String sushiname,int price){
        int confirmation = JOptionPane.showConfirmDialog(null,
                "Would you like to order "+sushiname+"?\n" +
                        "Price is "+price+" yen.",
                "Confirm Order",
                JOptionPane.YES_NO_OPTION);
//注文した回数をカウント、名前が同じときインクリメントする処理
        if(confirmation == 0) {
            for(int i=0;i<6;i++) {
                if (sushiname == menu[i])
                    cuntsushi[i]++;
            }

            //確認画面に注文確認を表示、確定前の価格を保存
            String detailsText = orderDetails.getText();
            orderDetails.setText(detailsText+sushiname+" :"+price+"yen"+"\n");
            preconfirmprice += price;

        }

    }
    //注文を確定した時の処理
    public void orderConfirmed() {
        int orderConfirmed = JOptionPane.showConfirmDialog(
                null,
                "The total price is " + preconfirmprice + " yen.",
                "Order Confirmed",
                JOptionPane.YES_NO_OPTION
        );
        //確定する前にカウントしたすしと今まで注文したすしを足し、注文履歴画面に表示
        if (orderConfirmed == 0) {

            for(int i=0;i<6;i++)
                confirmedcuntsushi[i] += cuntsushi[i];
            orderHistory.setText(
                    menu[0]+"   ×   "+confirmedcuntsushi[0]+"\n"+
                            menu[1]+"      ×   "+confirmedcuntsushi[1]+"\n"+
                            menu[2]+"      ×   "+confirmedcuntsushi[2]+"\n"+
                            menu[3]+"  ×   "+confirmedcuntsushi[3]+"\n"+
                            menu[4]+"      ×   "+confirmedcuntsushi[4]+"\n"+
                            menu[5]+"      ×   "+confirmedcuntsushi[5]+"\n"
            );
            //注文確認画面をリセットし合計金額を表示、確認用の値段を初期化
            orderDetails.setText(null);
            totalprice += preconfirmprice;
            totalText.setText("Total Price : "+totalprice+"yen");
            for(int i=0;i<6;i++)
                confirmedcuntsushi[i] = 0;
            preconfirmprice = 0;
        }
    }
    //キャンセルした時の処理
    public void Cancel() {
        int cancel = JOptionPane.showConfirmDialog(
                null,
                "Do you want to cansel your order?",
                "Cansel Order",
                JOptionPane.YES_NO_OPTION
        );
        //カウントしたすし、テキスト、合計金額を初期化
        for(int i=0;i<6;i++)
            cuntsushi[i] = 0;

        preconfirmprice = 0;
        orderDetails.setText(null);

    }


//チェックアウトした時の処理
    public void CheckOut() {
        int checkout = JOptionPane.showConfirmDialog(
                null,
                "Thank you. The total price is " + totalprice + " yen.",
                "Thank You!!",
                JOptionPane.YES_NO_OPTION
        );
        //カウントしたすし、テキスト、合計金額を初期化
        if (checkout == 0) {
            for(int i=0;i<6;i++){
                cuntsushi[i] = 0;
            }
            totalprice = 0;
            orderDetails.setText(null);
            orderHistory.setText(null);
            totalText.setText(null);
        }
    }



    public SushiOrderingMachine() {

        //注文ボタンに画像を挿入
        Button0.setIcon(new ImageIcon(
                this.getClass().getResource("otoro.png")
        ));

        Button1.setIcon(new ImageIcon(
                this.getClass().getResource("tyutoro.png")
        ));

        Button2.setIcon(new ImageIcon(
                this.getClass().getResource("tuna.png")
        ));


        Button3.setIcon(new ImageIcon(
                this.getClass().getResource("tyawanmushi.png")
        ));

        Button4.setIcon(new ImageIcon(
                this.getClass().getResource("icecream.png")
        ));

        Button5.setIcon(new ImageIcon(
                this.getClass().getResource("shotyu.png")
        ));



//それぞれのボタンに機能を付ける、order,cancel,confirmed,cheakoutの関数を呼び出す
        Button0.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                order(menu[0],price[0]);
            }
        });


        Button1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                order(menu[1],price[1]);

            }
        });


        Button2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                order(menu[2],price[2]);

            }
        });


        Button3.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                order(menu[3],price[3]);

            }
        });


        Button4.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                order(menu[4],price[4]);

            }
        });



        Button5.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                order(menu[5],price[5]);
            }
        });

        cancelButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                Cancel();

            }
        });

        orderConfirmedButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                orderConfirmed();

            }
        });

        cheakoutbutton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                CheckOut();

            }
        });
    }
    public static void main(String[] args) {
        JFrame frame = new JFrame("SushiOrderingMachine");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setContentPane(new SushiOrderingMachine().root);
        frame.pack();
        frame.setVisible(true);
    }

}

